﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonService;
namespace DataBaseToDocument
{
    public partial class Form1 : Form
    {
        // LAPTOP-LV16R3PR\SQL2012
        //IBaseService service = new BaseServiceMysql();
        IBaseService service;
        NpoiToDoc docservice = new NpoiToDoc();
        public static string Form1Value; // 注意，必须申明为static变量
        public Form1()
        {
            InitializeComponent();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            string servername = txtServer.Text.Trim();
            string uid = txtUser.Text.Trim();
            string pwd = txtPwd.Text.Trim();
            string constr = service.GetConnectioning(servername,uid,pwd);
                                
            if (service.ConnectionTest(constr))
            {
                MessageBox.Show("连接数据库成功！");
                comboBox1.DataSource = service.GetDBNameList(constr);         
            }
            else
            {
                MessageBox.Show("连接数据库失败！");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox2.SelectedIndex = 0;
            txtPwd.Text = "123456";
            txtServer.Text = "LAPTOP-LV16R3PR\\SQL2012";
            txtUser.Text = "sa";
        }

        private void btnToDoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.SelectedValue == null)
                {
                    MessageBox.Show("请选择数据库");
                    string servername = txtServer.Text.Trim();
                    string uid = txtUser.Text.Trim();
                    string pwd = txtPwd.Text.Trim();
                    string constr = service.GetConnectioning(servername, uid, pwd);
                    if (service.ConnectionTest(constr))
                    {
                        comboBox1.DataSource = service.GetDBNameList(constr);
                    }
                }
                else
                {
                    string db = comboBox1.SelectedValue.ToString();
                    string servername = txtServer.Text.Trim();
                    string uid = txtUser.Text.Trim();
                    string pwd = txtPwd.Text.Trim();
                    string constr = service.GetConnectioning(servername, uid, pwd, db);
                    var listnew = service.GetTableDetail("UserInfo", constr);
                    var list = service.GetDBTableList(constr, db);

                    docservice.CreateToWord(list, constr, db, comboBox2.SelectedIndex);
                }
            }
            catch
            {
                MessageBox.Show("内部错误！");
            }         

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedValue == null)
            {
                MessageBox.Show("请先保证服务器连接成功");
            }            
            else
            {
                if (comboBox2.SelectedIndex == 0)
                {
                    string db = comboBox1.SelectedValue.ToString();
                    string servername = txtServer.Text.Trim();
                    string uid = txtUser.Text.Trim();
                    string pwd = txtPwd.Text.Trim();
                    string constr = service.GetConnectioning(servername, uid, pwd, db);
                    Form1Value = constr;
                    //this.Hide();
                    FormToBak fr = new FormToBak();
                    fr.ShowDialog();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("导出数据库备份文件只支持本地服务器的SQLServer版本");
                }
            }

        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox2.SelectedIndex==0) //sqlserver
            {
                txtPwd.Text = "123456";
                txtServer.Text = "LAPTOP-LV16R3PR\\SQL2012";
                txtUser.Text = "sa";
                service = new BaseService();
            }
            else
            {
                txtPwd.Text = "root";
                txtServer.Text = "127.0.0.1";
                txtUser.Text = "root";
                service = new BaseServiceMysql();
            }
        }
    }
}
